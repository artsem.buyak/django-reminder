.EXPORT_ALL_VARIABLES:
COMPOSE_FILE ?= docker/docker-compose-local.yml
COMPOSE_PROJECT_NAME ?= django_reminder

DOTENV_BASE_FILE ?= .env-base
DOTENV_CUSTOM_FILE ?= env/.env

POETRY_EXPORT_WITHOUT_INDEXES ?= true
POETRY_EXPORT_OUTPUT = requirements.txt
POETRY_VERSION = 1.3.2
POETRY ?= $(HOME)/.local/bin/poetry

PYTHON_INSTALL_PACKAGES_USING ?= poetry
MANAGE_PY_PATH = src/manage.py

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

export PYTHONPATH=:$(CURDIR)/src:$(mkfile_dir):$(mkfile_dir)/src

-include $(DOTENV_BASE_FILE)
-include $(DOTENV_CUSTOM_FILE)

.PHONY: install-poetry
install-poetry:
	curl -sSL https://install.python-poetry.org | python3 -
	$(POETRY) config virtualenvs.create true
	$(POETRY) run pip install --upgrade pip

.PHONY: install-packages
install-packages:
	$(POETRY) install -vv $(opts)

.PHONY: install
install: install-poetry install-packages

.PHONY: activate
activate:
	$(POETRY) shell

.PHONY: remove
remove:
	$(POETRY) env remove $(ENV_NAME)

.PHONY: django_shell
django_shell:
	$(POETRY) run python $(MANAGE_PY_PATH) shell

.PHONY: update
update:
	$(POETRY) update -v

.PHONY: migrate
migrate:
	$(POETRY) run python $(MANAGE_PY_PATH)  migrate

# command example: `make downgrade APP_NAME='notifications' MIGRATION='0001'`
.PHONY: downgrade
downgrade:
	$(POETRY) run python $(MANAGE_PY_PATH)  migrate ${APP_NAME} ${MIGRATION}

.PHONY: migrations
migrations:
	$(POETRY) run python $(MANAGE_PY_PATH)  makemigrations

.PHONY: service
service:
	$(POETRY) run python $(MANAGE_PY_PATH)  runserver

.PHONY: docker-lint
docker-lint:
	docker run --rm -i replicated/dockerfilelint:ad65813 < ./docker/Dockerfile

.PHONY: docker-build
docker-build:
	@docker build \
		--tag=django_reminder \
		--file=docker/Dockerfile \
		--build-arg BUILD_RELEASE=dev \
		.


.PHONY: docker-build-m1
docker-build-m1:
	@docker buildx build --platform linux/amd64 \
		--tag=django_reminder \
		--file=docker/Dockerfile \
		--build-arg BUILD_RELEASE=dev \
		.


.PHONY: docker-up
docker-up:
	docker-compose up --remove-orphans -d
	docker-compose ps

.PHONY: docker-down
docker-down:
	docker-compose down

.PHONY: docker-logs
docker-logs:
	docker-compose logs --follow

.PHONY: lint-bandit
lint-bandit:
	$(POETRY) run bandit --ini .bandit --recursive

.PHONY: lint-black
lint-black:
	$(POETRY) run black --check --diff .

.PHONY: lint-flake8
lint-flake8:
	$(POETRY) run flake8

.PHONY: lint-isort
lint-isort:
	$(POETRY) run isort --check-only --diff .

.PHONY: lint
lint: lint-bandit lint-black lint-flake8 lint-isort

.PHONY: fmt
fmt:
	$(POETRY) run isort .
	$(POETRY) run black .

.PHONY: collectstatic
collectstatic:
	$(POETRY) run python $(MANAGE_PY_PATH)  collectstatic --no-input --clear

.PHONY: tests
tests:
	$(POETRY) run python -W ignore::RuntimeWarning $(MANAGE_PY_PATH)  test
