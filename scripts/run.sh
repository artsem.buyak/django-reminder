#!/usr/bin/env bash

: "${UWSGI_PORT:=8080}"
: "${UWSGI_PROCESSES:=2}"
: "${UWSGI_HARAKIRI:=600}"
: "${UWSGI_CHMOD:=666}"
: "${UWSGI_IDLE:=3600}"
: "${UWSGI_BUFFER_SIZE:=65536}"
: "${UWSGI_POST_BUFFERING:=8192}"
: "${UWSGI_MAX_REQUESTS:=10000}"
: "${UWSGI_LISTEN:=1024}"

exec uwsgi --socket=0.0.0.0:${UWSGI_PORT} \
      --master \
      --processes=${UWSGI_PROCESSES} \
      --harakiri=${UWSGI_HARAKIRI} \
      --wsgi-file=${APP_DIR}/src/wsgi.py \
      --chmod=${UWSGI_CHMOD} \
      --idle=${UWSGI_IDLE} \
      --buffer-size=${UWSGI_BUFFER_SIZE} \
      --post-buffering=${UWSGI_POST_BUFFERING} \
      --max-requests=${UWSGI_MAX_REQUESTS} \
      --listen=${UWSGI_LISTEN} \
      --stats=${APP_DIR}/uwsgi/stats.sock \
      --log-format='"method": "%(method)", "uri": "%(uri)", "http_version": "%(proto)", "http_status": %(status), "referer": "%(referer)", "user_agent": "%(uagent)", "remote_user": "%(user)", "remote_addr": "%(addr)", "http_host": "%(host)", "pid": %(pid), "worker_id": %(wid), "cgi_var": %(vars), "cgi_size": %(pktsize), "res_size": %(rsize), "core": %(core), "core_switches": %(switches), "io_errors": %(ioerr), "rq_size": %(cl), "res_time": %(msecs), "rs_size": %(size), "headers_size": %(hsize), "headers": %(headers)' \
      --log-encoder='format {"timestamp": "${strftime:%Y-%m-%dT%H:%M:%S}", "event": "uwsgi_request", ${msg}}' \
      --log-encoder=nl \
      --hook-master-start "unix_signal:15 gracefully_kill_them_all" \
      --shutdown-socket \
      --need-app
