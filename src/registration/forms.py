from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    username = forms.CharField(
        label="fasdfasdfsd",
        max_length=30,
        min_length=5,
        required=True,
        widget=forms.TextInput(attrs={"placeholder": "Username", "class": "form-control"}),
    )

    email = forms.EmailField(
        label="",
        max_length=255,
        required=True,
        widget=forms.EmailInput(attrs={"placeholder": "Email", "class": "form-control"}),
    )

    first_name = forms.CharField(
        label="",
        max_length=30,
        min_length=5,
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "First name", "class": "form-control"}),
    )

    last_name = forms.CharField(
        label="",
        max_length=30,
        min_length=5,
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "Last name", "class": "form-control"}),
    )

    password1 = forms.CharField(
        label="",
        max_length=30,
        min_length=8,
        required=True,
        widget=forms.PasswordInput(attrs={"placeholder": "Password", "class": "form-control"}),
    )

    password2 = forms.CharField(
        label="",
        max_length=30,
        min_length=8,
        required=True,
        widget=forms.PasswordInput(attrs={"placeholder": "Confirm Password", "class": "form-control"}),
    )

    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name", "password1", "password2")

    def clean_email(self):
        email = self.cleaned_data.get("email")
        username = self.cleaned_data.get("username")
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError("Email addresses must be unique.")
        return email


class UpdateUserForm(forms.ModelForm):
    username = forms.CharField(
        label="",
        max_length=30,
        min_length=5,
        required=True,
        widget=forms.TextInput(attrs={"placeholder": "Username", "class": "form-control"}),
    )

    email = forms.EmailField(
        label="",
        max_length=255,
        required=True,
        widget=forms.EmailInput(attrs={"placeholder": "Email", "class": "form-control"}),
    )

    first_name = forms.CharField(
        label="",
        max_length=30,
        min_length=5,
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "First name", "class": "form-control"}),
    )

    last_name = forms.CharField(
        label="",
        max_length=30,
        min_length=5,
        required=False,
        widget=forms.TextInput(attrs={"placeholder": "Last name", "class": "form-control"}),
    )

    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name")

    def clean_email(self):
        email = self.cleaned_data.get("email")
        username = self.cleaned_data.get("username")
        if email and User.objects.filter(email=email).exclude(username=username).exists():
            raise forms.ValidationError("Email addresses must be unique.")
        return email
