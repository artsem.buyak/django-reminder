from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView
from django.views.generic.edit import CreateView

from .forms import SignUpForm, UpdateUserForm


class SignUpView(SuccessMessageMixin, CreateView):
    template_name = "signup"
    success_url = reverse_lazy("login")
    form_class = SignUpForm
    success_message = "Your User was created successfully!"


class UserUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = User
    template_name = "user_edit"
    success_url = reverse_lazy("home_page")
    form_class = UpdateUserForm
    success_message = "Your User info was changed successfully!"

    def get_object(self, queryset=None):
        return User.objects.get(pk=self.request.user.pk)
