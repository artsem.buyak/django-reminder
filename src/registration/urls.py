from django.contrib.auth.views import LoginView, LogoutView
from django.urls import re_path

from .views import SignUpView, UserUpdateView

urlpatterns = (
    re_path(
        r"^login/",
        LoginView.as_view(template_name="registration/login.html"),
        name="login",
    ),
    re_path(
        r"^logout/",
        LogoutView.as_view(template_name="registration/logout.html"),
        name="logout",
    ),
    re_path(
        r"^signup/",
        SignUpView.as_view(template_name="registration/signup.html"),
        name="signup",
    ),
    re_path(
        r"user/edit/",
        UserUpdateView.as_view(template_name="application/user_edit.html"),
        name="user_edit",
    ),
)
