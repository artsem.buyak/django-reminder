from django.urls import re_path

from src.main_app.views import HomePageView, NotificationsListView

urlpatterns = (
    re_path(
        r"dashboard/",
        NotificationsListView.as_view(template_name="application/dashboard.html"),
        name="dashboard",
    ),
    re_path("", HomePageView.as_view(), name="home_page"),
)
