from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import ListView
from django.views.generic.base import TemplateView

from .models import NotificationTasks


class HomePageView(TemplateView):
    template_name = "general/base.html"


class NotificationsListView(LoginRequiredMixin, ListView):
    model = NotificationTasks
    template_name = "application/dashboard"

    def get_queryset(self):
        queryset = User.objects.get(pk=self.request.user.pk).notifications.all().order_by("-date_of_coming")
        return queryset
