from datetime import datetime

from django.contrib.auth.models import User
from django.db import models


class NotificationTasks(models.Model):
    title = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(blank=True, null=True)
    location = models.CharField(max_length=255)
    members = models.ManyToManyField(User, related_name="notifications", blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_of_coming = models.DateTimeField(default=datetime.now, db_index=True)
    creator = models.ForeignKey(User, related_name="creators", on_delete=models.CASCADE, null=True)
    is_finished = models.BooleanField(default=False, db_index=True)
    celery_task_id = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        indexes = [
            models.Index(fields=["is_finished", "date_of_coming"]),
        ]
