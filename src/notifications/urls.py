from django.urls import re_path

from .views import (
    NotificationCreateView,
    NotificationDeleteView,
    NotificationDetailView,
    NotificationUpdateView,
)

urlpatterns = (
    re_path(
        r"notification/edit/(?P<pk>[0-9]+)/",
        NotificationUpdateView.as_view(template_name="notifications/edit.html"),
        name="notification_edit",
    ),
    re_path(
        r"notification/view/(?P<pk>[0-9]+)/",
        NotificationDetailView.as_view(template_name="notifications/view.html"),
        name="notification_view",
    ),
    re_path(r"notification/remove/(?P<pk>[0-9]+)/", NotificationDeleteView.as_view(), name="notification_remove"),
    re_path(
        r"create_notification/",
        NotificationCreateView.as_view(template_name="notifications/create_notification.html"),
        name="create_notification",
    ),
)
