from datetime import datetime

from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.models import Q

from src.main_app.models import NotificationTasks


class UpdateNotificationForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        self.creator = user
        super().__init__(*args, **kwargs)

    date_of_coming = forms.DateTimeField(input_formats=["%Y-%m-%dT%H:%M"])

    class Meta:
        model = NotificationTasks
        fields = ("title", "location", "description", "members", "date_of_coming", "is_finished")

    def clean_members(self):
        if "members" not in self.data:
            return User.objects.filter(id=self.creator.id)

        members = self.data["members"]
        # add creator to recipients list
        users_queryset = User.objects.filter(Q(id__in=members) | Q(id=self.creator.id))
        if not set(members).issubset({str(user.id) for user in users_queryset}):
            raise ValidationError("Recipients contains is not registered users!")

        return users_queryset

    def clean_date_of_coming(self):
        if datetime.now() > datetime.strptime(self.data["date_of_coming"], "%Y-%m-%dT%H:%M") and not self.data.get(
            "is_finished"
        ):
            raise ValidationError("Please, set date in future, not in the past")

        return self.data["date_of_coming"]


class CreateNotificationForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        self.creator = user
        super().__init__(*args, **kwargs)

    date_of_coming = forms.DateTimeField(input_formats=["%Y-%m-%dT%H:%M"])

    class Meta:
        model = NotificationTasks
        fields = ("title", "location", "description", "members", "date_of_coming")

    def clean_members(self):
        if "members" not in self.data:
            return User.objects.filter(id=self.creator.id)

        members = self.data["members"]
        # add creator to recipients list
        users_queryset = User.objects.filter(Q(id__in=members) | Q(id=self.creator.id))
        if not set(members).issubset({str(user.id) for user in users_queryset}):
            raise ValidationError("Recipients contains is not registered users!")

        return users_queryset

    def clean_date_of_coming(self):
        if datetime.now() > datetime.strptime(self.data["date_of_coming"], "%Y-%m-%dT%H:%M") and not self.data.get(
            "is_finished"
        ):
            raise ValidationError("Please, set date in future, not in the past")

        return self.data["date_of_coming"]
