from celery import shared_task
from django.conf import settings
from django.core.mail import send_mail
from django.utils.timezone import now

from src.main_app.models import NotificationTasks


@shared_task(queue="notifications_tasks", routing_key="notifications_task.send")
def send_email_notifications(notification_id: int) -> None:
    notification = NotificationTasks.objects.filter(id=notification_id).first()

    if not notification:
        return

    send_mail(
        subject=notification.title,
        message=notification.description,
        from_email=settings.EMAIL_HOST_USER,
        auth_user=settings.EMAIL_HOST_USER,
        auth_password=settings.EMAIL_HOST_PASSWORD,
        recipient_list=list(notification.members),
        fail_silently=True,
    )

    notification.is_finished = True
    notification.save()


@shared_task
def scan_notifications_tasks() -> None:
    for notification_id in NotificationTasks.objects.filter(
        is_finished=False,
        date_of_coming__lte=now(),
    ).values_list("id", flat=True):
        send_email_notifications.apply_async(
            args=[
                notification_id,
            ],
            queue="notifications_tasks",
        )
