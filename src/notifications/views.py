from datetime import datetime

import pytz
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView

from src.main_app.models import NotificationTasks

from .forms import CreateNotificationForm, UpdateNotificationForm


class NotificationUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = NotificationTasks
    template_name = "notifications/edit"
    success_url = reverse_lazy("dashboard")
    form_class = UpdateNotificationForm
    success_message = "Your Notification info was changed successfully!"

    def get_context_data(self, **kwargs):
        data = super(NotificationUpdateView, self).get_context_data()
        instance = self.get_object()

        data.update(
            {
                "object": instance,
                "users_list": User.objects.exclude(id=self.request.user.id),
                "date_of_coming": timezone.localtime(instance.date_of_coming, pytz.timezone("Europe/Minsk")).strftime(
                    "%Y-%m-%dT%H:%M"
                ),
            }
        )
        return data

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view."""
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(user=self.request.user, **self.get_form_kwargs())

    def form_valid(self, form):
        response = super().form_valid(form=form)
        self.object.refresh_from_db()

        return response


class NotificationDetailView(LoginRequiredMixin, DetailView):
    model = NotificationTasks
    template_name = "notifications/view"


class NotificationDeleteView(LoginRequiredMixin, DeleteView):
    model = NotificationTasks
    success_url = reverse_lazy("dashboard")
    template_name = "notifications/notification_confirm_delete"
    success_message = "Your Notification removed successfully!"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        response = super().delete(request, *args, **kwargs)

        return response


class NotificationCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    template_name = "create_notification"
    success_url = reverse_lazy("dashboard")
    form_class = CreateNotificationForm
    success_message = "Your Notification was created successfully!"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data.update(
            {
                "users_list": User.objects.exclude(id=self.request.user.id),
                "date_of_coming": datetime.now(tz=timezone.get_current_timezone()).strftime("%Y-%m-%dT%H:%M"),
            }
        )
        return data

    def form_valid(self, form):
        response = super().form_valid(form=form)
        self.object.creator = self.request.user
        self.object.save()
        self.object.refresh_from_db()

        return response

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view."""
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(user=self.request.user, **self.get_form_kwargs())
