from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from src.main_app.models import NotificationTasks, User
from src.public_api import serializers
from src.public_api.permissions import IsNotificationOwner


class NotificationsViewSet(ModelViewSet):
    serializer_class = serializers.NotificationSerializer
    model = NotificationTasks
    permission_classes = (IsAuthenticated,)

    def get_permissions(self):
        if self.action in ["retrieve", "destroy"]:
            return [IsAuthenticated(), IsNotificationOwner()]

        return super().get_permissions()

    def get_queryset(self):
        user = self.request.user
        queryset = self.model.objects.filter(members__id=user.id).prefetch_related("members")

        return queryset

    def create(self, request, *args, **kwargs):
        serializer_class = serializers.NotificationCreateSerializer
        kwargs.setdefault("context", self.get_serializer_context())
        data = request.data.dict()
        data["members"] = (
            [self.request.user.email, *data.get("members", [])]
            if not isinstance(data.get("members", []), str)
            else [self.request.user.email, data.get("members", [])]
        )

        kwargs["data"] = data

        serializer = serializer_class(*args, **kwargs)

        serializer.is_valid()

        notification = self.model.objects.create(
            title=serializer.validated_data.get("title"),
            location=serializer.validated_data.get("location"),
            description=serializer.validated_data.get("description"),
            date_of_coming=serializer.validated_data.get("date_of_coming"),
            creator=self.request.user,
        )
        notification.members.add(*User.objects.filter(email__in=serializer.validated_data.get("members")))

        return Response(serializer.data, status=status.HTTP_201_CREATED)
