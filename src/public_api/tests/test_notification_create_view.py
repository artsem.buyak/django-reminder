from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from src.main_app.models import NotificationTasks


class NotificationDetailTestCase(TestCase):
    def setUp(self):
        username = "django_reminder_test_user"
        password = "querty123"  # nosec
        self.user = User.objects.create_user(
            username=username, password=password, email="django_remider@mail.ru"
        )  # nosec
        self.client.login(username=username, password=password)

    def test__ok__create_notification(self):
        User.objects.create_user(username="test", password="123", email="test@test.py")  # nosec
        title = "Test Api Value Description"
        response = self.client.post(
            reverse("notifications-list"),
            data={
                "date_of_coming": "2020-10-05T11:20:00+03:00",
                "description": "Test Api Value Desription",
                "location": "Test Api Value Description",
                "members": "test@test.py",
                "title": "Test Api Value Description",
            },
        )

        self.assertEqual(response.status_code, 201)
        self.assertTrue(NotificationTasks.objects.filter(creator=self.user, title=title).exists())

    def test__fail__create_authentication_error(self):
        self.client.logout()

        response = self.client.post(
            reverse("notifications-list"),
            data={
                "date_of_coming": "2020-10-05T11:20:00+03:00",
                "description": "Test Api Value Desription",
                "location": "Test Api Value Description",
                "title": "Test Api Value Description",
            },
        )
        self.assertEqual(response.status_code, 401)

        self.assertEqual(response.json(), {"detail": "Authentication credentials were not provided."})
