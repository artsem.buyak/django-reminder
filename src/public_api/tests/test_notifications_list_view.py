from datetime import timedelta

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from src.main_app.models import NotificationTasks


class NotificationListTestCase(TestCase):
    def setUp(self):
        username = "django_reminder_test_user"
        password = "querty123"  # nosec
        self.user = User.objects.create_user(
            username=username, password=password, email="django_remider@mail.ru"
        )  # nosec
        self.client.login(username=username, password=password)

    def test__ok__notifications_list_without_members(self):
        response = self.client.get(reverse("notifications-list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

        notification = NotificationTasks.objects.create(
            title="Title",
            description="Test value",
            location="Test location",
            creator=self.user,
        )
        notification.members.add(self.user)
        date = notification.date_of_coming.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = self.client.get(reverse("notifications-list"))

        notification.refresh_from_db()
        date_created = (notification.date_created + timedelta(hours=3)).strftime("%Y-%m-%dT%H:%M:%S.%f")
        self.assertEqual(
            response.json(),
            [
                {
                    "id": notification.id,
                    "date_of_coming": f"{date}+03:00",
                    "members": [{"username": self.user.username, "email": self.user.email}],
                    "title": notification.title,
                    "description": notification.description,
                    "location": notification.location,
                    "creator": {"username": self.user.username, "email": self.user.email},
                    "is_finished": notification.is_finished,
                    "date_created": f"{date_created}+03:00",
                }
            ],
        )

    def test__ok__notifications_list_with_members(self):
        response = self.client.get(reverse("notifications-list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

        user_2 = User.objects.create_user(username="test", password="123", email="django_remider_2@mail.ru")  # nosec

        notification = NotificationTasks.objects.create(
            title="Title",
            description="Test value",
            location="Test location",
            creator=self.user,
        )
        notification.members.add(*[self.user, user_2])
        date = notification.date_of_coming.strftime("%Y-%m-%dT%H:%M:%S.%f")
        response = self.client.get(reverse("notifications-list"))

        date_created = (notification.date_created + timedelta(hours=3)).strftime("%Y-%m-%dT%H:%M:%S.%f")
        self.assertEqual(
            response.json(),
            [
                {
                    "id": notification.id,
                    "date_of_coming": f"{date}+03:00",
                    "members": [
                        {"username": self.user.username, "email": self.user.email},
                        {"username": user_2.username, "email": user_2.email},
                    ],
                    "title": notification.title,
                    "creator": {"username": self.user.username, "email": self.user.email},
                    "description": notification.description,
                    "location": notification.location,
                    "is_finished": notification.is_finished,
                    "date_created": f"{date_created}+03:00",
                }
            ],
        )

    def test__fail__authentication_error(self):
        self.client.logout()

        response = self.client.get(reverse("notifications-list"))
        self.assertEqual(response.status_code, 401)

        self.assertEqual(response.json(), {"detail": "Authentication credentials were not provided."})
