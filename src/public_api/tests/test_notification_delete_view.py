from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from src.main_app.models import NotificationTasks


class NotificationDetailTestCase(TestCase):
    def setUp(self):
        username = "django_reminder_test_user"
        password = "querty123"  # nosec
        self.user = User.objects.create_user(
            username=username, password=password, email="django_remider@mail.ru"
        )  # nosec
        self.client.login(username=username, password=password)

    def test__ok__delete_notification(self):
        notification = NotificationTasks.objects.create(
            title="Title",
            description="Test value",
            location="Test location",
            creator=self.user,
        )
        notification.members.add(self.user)
        response = self.client.delete(reverse("notifications-detail", kwargs={"pk": notification.id}))

        self.assertEqual(response.status_code, 204)
        self.assertFalse(NotificationTasks.objects.filter(id=notification.id).exists())

    def test__fail__delete_authentication_error(self):
        self.client.logout()
        notification = NotificationTasks.objects.create(
            title="Title",
            description="Test value",
            location="Test location",
            creator=self.user,
        )

        response = self.client.delete(reverse("notifications-detail", kwargs={"pk": notification.id}))
        self.assertEqual(response.status_code, 401)

        self.assertEqual(response.json(), {"detail": "Authentication credentials were not provided."})

    def test__fail__delete_not_owner_error(self):
        user_2 = User.objects.create_user(username="test", password="123", email="django_remider_2@mail.ru")  # nosec
        notification = NotificationTasks.objects.create(
            title="Title",
            description="Test value",
            location="Test location",
            creator=user_2,
        )

        response = self.client.delete(reverse("notifications-detail", kwargs={"pk": notification.id}))
        self.assertEqual(response.status_code, 403)

        self.assertEqual(response.json(), {"detail": "You do not have permission to perform this action."})
