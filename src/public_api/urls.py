from rest_framework.routers import SimpleRouter

from src.public_api.views import NotificationsViewSet

router = SimpleRouter(trailing_slash=False)

router.register("notifications", NotificationsViewSet, basename="notifications")

urlpatterns = router.urls
