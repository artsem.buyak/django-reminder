from django.contrib.auth.models import User
from rest_framework import serializers

from src.main_app.models import NotificationTasks


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "email")


class NotificationSerializer(serializers.ModelSerializer):
    members = UserSerializer(source="members.all", many=True)
    creator = UserSerializer(read_only=True)

    class Meta:
        model = NotificationTasks
        fields = (
            "id",
            "creator",
            "date_created",
            "date_of_coming",
            "title",
            "description",
            "location",
            "members",
            "is_finished",
        )
        extra_kwargs = {
            "id": {"read_only": True, "help_text": "instance id notification task"},
            "date_created": {"help_text": "datetime of creation a notification"},
            "date_of_coming": {"help_text": "datetime of delivery message"},
            "title": {"help_text": "title of email notification"},
            "description": {"help_text": "body of message"},
            "location": {"help_text": "location(room/office or other)"},
            "members": {"help_text": "list of recipients (aka emails)"},
            "is_finished": {"help_text": "flag delivered or not"},
        }

    def validate_members(self, members):
        if isinstance(members[0], list):
            members = members[0]
        if not isinstance(members, list):
            raise serializers.ValidationError("Is not a list.")
        if User.objects.filter(email__in=members).count() != len(members):
            raise serializers.ValidationError("Recipients contains is not registered users!")

        return members


class NotificationCreateSerializer(serializers.ModelSerializer):
    members = serializers.ListField()

    class Meta:
        model = NotificationTasks
        fields = ("date_of_coming", "title", "description", "location", "members")

    def validate_members(self, members):
        if isinstance(members[0], list):
            members = members[0]
        if not isinstance(members, list):
            raise serializers.ValidationError("Is not a list.")
        if User.objects.filter(email__in=members).count() != len(members):
            raise serializers.ValidationError("Recipients contains is not registered users!")

        return members
