from rest_framework.permissions import BasePermission

from src.main_app.models import NotificationTasks


class IsNotificationOwner(BasePermission):
    def has_permission(self, request, view):
        obj = NotificationTasks.objects.filter(
            pk=request.parser_context.get("kwargs", {}).get(view.lookup_field)
        ).first()

        return obj and obj.creator == request.user
