from django.contrib import admin
from django.urls import include, re_path

urlpatterns = [
    re_path(r"^admin/", admin.site.urls),
    re_path(r"^api/v1/", include("src.public_api.urls")),
    re_path(r"", include("src.registration.urls")),
    re_path(r"", include("src.notifications.urls")),
    re_path(r"", include("src.main_app.urls")),
]
