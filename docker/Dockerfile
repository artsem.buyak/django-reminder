FROM python:3.11.1-slim
LABEL maintainer="artsem.buyak@gmail.com"


RUN apt-get update && \
    apt-get -y install --no-install-recommends curl make && \
    rm -rf /var/lib/apt/lists/*


ARG APP_DIR=/var/app

ENV PATH=/root/.local/bin:${PATH} \
    PIP_NO_CACHE_DIR=off \
    POETRY_VERSION=1.3.2 \
    POETRY_VIRTUALENVS_CREATE=false

RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR $APP_DIR

COPY entrypoints/docker/entrypoint.sh Makefile pyproject.toml poetry.lock README.md ./
RUN mkdir src
RUN mkdir static
RUN make install-packages opts="--no-dev"

COPY src/ ${APP_DIR}/src/
COPY static/ ${APP_DIR}/static/

COPY .env-base ${APP_DIR}/

WORKDIR ${APP_DIR}
RUN make collectstatic && rm .env-base

ARG BUILD_RELEASE
ENV BUILD_RELEASE=${BUILD_RELEASE}
ENV PYTHONPATH="$PYTHONPATH:/var/app/src"

CMD ["/bin/bash"]
